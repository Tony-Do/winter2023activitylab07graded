import java.util.Scanner;
public class TicTacToeGame
{
	public static void main(String[]args)
	{
		Scanner scan = new Scanner(System.in);
		System.out.println("Welcome to a Tic-Tac-Toe Game!");
		
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		
		Square playerToken = Square.X;
		
		
		
		while(!gameOver)
		{
			System.out.println(board);
			if(player == 1)
			{
				playerToken = Square.X;
				
			}
			else
			{
				playerToken = Square.O;
				
			}
			System.out.println("Player " + player + ", please input your row first then your column ");
			int row = scan.nextInt();
			int col = scan.nextInt();
			
			
			while (!board.placeToken(row,col,playerToken))
			{
				System.out.println("Re-input your row and column");
				 row = scan.nextInt();
				 col = scan.nextInt();
				
			}
			
			if(board.checkIfFull())
			{
				System.out.println("It's a tie!");
				gameOver = true;
			}
			else if(board.checkIfWinning(playerToken))
			{
				System.out.println("Player " + player + " wins! ");
				gameOver = true;
			}
			else
			{
				player+=1;
				if(player > 2)
				{
					player = 1;
				}
			}
		}
	}
	
	
	
	
	
	
}