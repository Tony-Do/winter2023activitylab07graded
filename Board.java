public class Board
{
	private Square[][] tictactoeBoard;
	
	public Board()
	{
		tictactoeBoard = new Square[3][3];
		for(int rows = 0; rows < 3; rows++)
		{
			for(int columns = 0; columns < 3; columns++)
			{
				tictactoeBoard[rows][columns] = Square.BLANK;
			}
		}
	}



	@Override 
	public String toString()
	{
		String result = "";
		for(int i = 0; i < 3; i++)
		{
			for(int j = 0; j < 3; j++)
			{
				result+= tictactoeBoard[i][j].toString() + " ";
			}
		result += "\n";
		}
		return result;
	}
	
	
	public boolean placeToken(int row, int col, Square playerToken)
	{
		if(row < 0 || row >= 3 || col < 0 || col >= 3)
		{
			return false;
		}
		
		if(tictactoeBoard[row][col] != Square.BLANK)
		{
				return false;
		}
		
		tictactoeBoard[row][col] = playerToken;
		return true;
	}
	
	
	public boolean checkIfFull() 
	{
		for (int i = 0; i < 3; i++) 
		{
			for (int j = 0; j < 3; j++) 
			{
				if (tictactoeBoard[i][j] == Square.BLANK) 
				{
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Square playerToken) 
	{
		for (int row = 0; row < 3; row++) 
		{
			if (tictactoeBoard[row][0] == playerToken && tictactoeBoard[row][1] == playerToken && tictactoeBoard[row][2] == playerToken) 
			{
				return true;
			}
		}
		return false;
	}


	private boolean checkIfWinningVertical(Square playerToken) 
	{
		for (int col = 0; col < 3; col++) 
		{
			if (tictactoeBoard[0][col] == playerToken && tictactoeBoard[1][col] == playerToken && tictactoeBoard[2][col] == playerToken) 
			{
				return true;
			}
		}
		return false;
	}
	
	public boolean checkIfWinning(Square playerToken)
	{
		if(checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken))
		{
			return true;
		}
		return false;
	}

	
}

